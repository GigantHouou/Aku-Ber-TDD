from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
response = {}
def index(request):
	if 'user_login' in request.session:
		response['author'] = "Andika Hanavian Atmam"
		html = 'lab_6/lab_6.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect(reverse('lab-9:index'))
