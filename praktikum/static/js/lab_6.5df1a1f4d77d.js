$(document).ready(function(){
	/* jQuery code will be add later */
	//challenge for chatbox
	$(".chat-text").keypress(function(e){
		//Enter Button Checker(E.B.C)
		if(e.which == 13){
			e.preventDefault(); //akan dicegah default enter-nya
			var input = $("textarea").val(); //mengambil data di textarea
			$("textarea").val(""); //erasing...
			$(".msg-insert").append('<p class="msg-send">' +input+'</p>'+'<br/>'); //adding the new word in new chat
		}
	});
	$(".chat button").click(function(){
		var inp = $("textarea").val(); //mengambil data di textarea
		$("textarea").val(""); //erasing...
		$(".msg-insert").append('<p class="msg-send">'+inp+'</p>'+'<br/>'); //adding the new word in new chat
	})
	//menginisiasikan data yang berisi warna
	var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];
	//menginisiasikan data tema (yang default)
	var deftheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
	//activating default theme when that web has being opened
	if(localStorage.getItem("deftheme") === null){
		localStorage.setItem('deftheme', JSON.stringify(deftheme));
	} 
	//saving data about themes
	localStorage.setItem('themes', JSON.stringify(themes));
	//mengisi semua tema ke select2
	var retriver = localStorage.getItem('themes');
	$('.my-select').select2({data : JSON.parse(retriver)});
	//terapkan default theme yang ada di localStorage
	var retselect = JSON.parse(localStorage.getItem('deftheme'));
	var kunci;
	var bcgColor;
	var fontColor;
	for(kunci in retselect){
		if(retselect.hasOwnProperty(kunci)){
			bcgColor = retselect[kunci].bcgColor;
			fontColor = retselect[kunci].fontColor;
		}
	}
	$("body").css({"background-color": bcgColor});
	$("footer").css({"color": fontColor});
	//fungsi tombol "apply"
	$('.apply-button').on('click', function(){  // sesuaikan class button
		// [TODO] ambil value dari elemen select .my-select
		var valTheme = $('.my-select').val();
		// [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
		// [TODO] ambil object theme yang dipilih
		// [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
		// [TODO] simpan object theme tadi ke local storage selectedTheme
		var tema;
		var alp;
		var selTheme = {};
		//searching the theme and match its ID
		for(alp in themes){
			if(alp == valTheme){
				var bcgColor = themes[alp].bcgColor;
				var fontColor = themes[alp].fontColor;
				var txt = themes[alp].text;
				$("body").css({"background-color": bcgColor});
				$("footer").css({"color": fontColor});
				selTheme[txt] = {"bcgColor" : bcgColor, "fontColor" : fontColor};
				localStorage.setItem('deftheme', JSON.stringify(selTheme));
			}
		}
	})
});

// Calculator

var erase = false;
var go = function(x) {
	var print = document.getElementById('print');
  if (x === 'ac') {
    /* implemetnasi clear all */
	//mengubah angka sebelumnya menjadi nol
	print.value = "";
	erase = false;
  } 
  else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }
  else if(x === 'log'|| x === 'sin' || x === 'tan'){
	  /* implementasi sudut dan logaritma */
	  if(x === 'sin' || x === 'tan'){
		print.value = eval('Math.' + x + '(' + keDerajat(print.value) +')').toPrecision(1);
	  }
	  else{
		print.value = eval('Math.' + x +'10' + '(' + print.value + ')').toPrecision(1);
	  }
	  erase = true;
  }
  else {
	if(erase === true && (x !== ' * ' && x !== ' - ' && x !== ' + ' && x !== ' / ')){
		print.value = "";
	}  
    print.value += x;
	erase = false;
  }
};

function keDerajat(sudut){
	return sudut * Math.PI/180;
}
function evil(fn) {
  return new Function('return ' + fn)();
}
// END
$(document).ready(function(){
	$(".chat-head").click(function(){
		console.log("Hello")
		$(".chat-body").toggle();
	});
});
